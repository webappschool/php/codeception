# Was - Php - Codeception
Bienvenue dans le projet permettant d'apprendre à créer des tests d'acceptation, fonctionnels, unitaires, etc. avec [Codeception](https://codeception.com/). Vous trouverez les exercices liés au projet [ici](https://gitlab.com/webappschool/php/codeception/-/wikis/home).
## Installation
Vous avez besoin de [PHP7](https://www.php.net/manual/fr/intro-whatis.php) au minimum, de [Composer](https://getcomposer.org/), d'un serveur Web local ainsi que de [Git](https://git-scm.com/) pour récupérer le projet.
### Linux (Debian)
```bash
# Installation de PHP
User@Desktop:~$ sudo apt install php-common php-curl php-gd php-intl php-json php-mbstring php-xml php-zip

# Installation de Apache2 (Serveur Web)
User@Desktop:~$ sudo apt install libapache2-mod-php

# Installation de Nginx (Serveur Web)
User@Desktop:~$ sudo apt install nginx php-fpm
```
### Windows
Vous pouvez installer le serveur Web [WAMP](http://www.wampserver.com/), [UwAmp](https://www.uwamp.com/fr/) ou tout autre serveur Web compatible avec [PHP](https://www.php.net/manual/fr/intro-whatis.php).

Vous aurez aussi besoins de [Composer](https://getcomposer.org/download/) et de [Git](https://git-scm.com/download/win).
### MacOS
Vous pouvez installer le serveur Web [XAMPP](https://www.apachefriends.org/fr/download.html) ou tout autre serveur Web compatible avec [PHP](https://www.php.net/manual/fr/intro-whatis.php).

Vous aurez aussi besoins de [Composer](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-macos) et de [Git](https://git-scm.com/download/mac).
### Docker
Si vous êtes familier avec l'infrastructure [Docker](https://www.docker.com/) vous pouvez alors utiliser le `Makefile` à la racine du projet afin de construire et lancer une image contenant toutes les dépendances.

## Structure
Vous trouverez ci-dessous la liste des dossiers et fichiers importants :
- `src/` Contient le code source de l'application
- `tests/` Contient tout les éléments relatifs aux tests
- `vendor/` Contient toutes les dépendances [Composer](https://getcomposer.org/) dont vous avez besoins
- `.gitignore` Contient une liste de règles qui doivent être ignorer par [Git](https://git-scm.com/)
- `composer.json` Contient les informations du projet liés à [Composer](https://getcomposer.org/) ainsi que la liste des dépendances à installer.