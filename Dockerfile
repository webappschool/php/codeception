FROM toroia/php-fpm:7.3-dev

COPY . /app

RUN rm -rf /tmp/* /var/cache/apk/* \
  && find /var/log -type f | while read f; do echo -n '' > ${f}; done