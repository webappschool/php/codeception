WASNAME?=wasdockerapp
WASIMG?=wasdockerapp
WASPORT?=8045
WASEXEC?=bash

.DEFAULT_GOAL := help
help:
	@printf "\033[33mUsage:\033[0m\n  make [target] [arg=\"val\"...]\n\n\033[33mTargets:\033[0m\n"
	@grep -E '^[-a-zA-Z0-9_\.\/]+:.*?## .*$$' $(MAKEFILE_LIST) \
		| sort | awk 'BEGIN {FS = ":.*?## "}; {printf "  \033[32m%-15s\033[0m %s\n", $$1, $$2}'

serve: ## Démarre un serveur PHP FPM avec l'image Docker
	docker run --rm -d --name $(WASNAME) -e PHP_IDE_CONFIG="serverName=was" -v $(CURDIR):/app -p $(WASPORT):80 $(WASIMG)

build: ## Fabrique l'image PHP DPM Docker
	docker build -t $(WASIMG) .

stop: ## Stoppe le serveur PHP FPM utilisé via l'image Docker
	docker stop $(WASNAME)

exec: ## Execute Bash dans l'image Docker
	docker exec -it $(WASNAME) $(WASEXEC)

coverage: ## Execute le code coverage dans l'image Docker
	docker exec -it $(WASNAME) composer coverage

test: ## Effectue les tests unitaires PHPUnit avec une image Docker
	docker exec -it $(WASNAME) composer test

clean: ## Nettoie le cache et les fichiers de génération du projet
	rm -Rf ./vendor

