Dossiers contenant le code source de l'application :
- `app/` Contient les classes de l'application
- `public/` Contient la racine exposée par le serveur Web
- `tmp/` Contient des fichiers temporaires crée par le Framework
- `views/` Contient les vues de rendu