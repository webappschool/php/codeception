<?php /** @noinspection PhpIncludeInspection */
declare(strict_types=1);

define('BASEPATH', dirname(__DIR__, 2));
$racine = rtrim(str_replace('\\', '/', dirname($_SERVER['SCRIPT_NAME'], 3)), '/');
$_SERVER['REQUEST_URI'] = str_replace($racine, '', $_SERVER['REQUEST_URI']);
require(BASEPATH . '/vendor/autoload.php');

// Inclus le fichier c3.php qui permet de gérer la couverture de code
if (file_exists($c3Path = BASEPATH . '/c3.php')) {
    require($c3Path);
}

// Configuration de l'instance F3
$f3 = Base::instance();
$f3->set('baseUrl', $racine);
$f3->set('UI', BASEPATH . '/src/views/');
$f3->set('TEMP', BASEPATH . '/src/tmp/');

//TODO Vous pouvez créer les routes automatiquement (voir la documentation du framework)

// Création des routes F3 (manuellement)
$f3->route('GET /', 'Was\\TestsRecettes\\Controllers\Index->index');
$f3->route('GET /cgu', 'Was\\TestsRecettes\\Controllers\Cgu->index');
$f3->route('GET /shopify', 'Was\\TestsRecettes\\Controllers\Shopify->index');
$f3->route('GET /newsletter', 'Was\\TestsRecettes\\Controllers\Newsletter->index');
$f3->route('POST /newsletter', 'Was\\TestsRecettes\\Controllers\Newsletter->signin');

// Création des routes F3 - API
$f3->route('GET /api/articles', 'Was\\TestsRecettes\\Controllers\\Api\\Articles->index');
$f3->route('GET /api/articles/@action', 'Was\\TestsRecettes\\Controllers\\Api\\Articles->@action');

// Lancement de l'application
$f3->run();