<?php /** @noinspection PhpUnused */
declare(strict_types=1);

namespace Was\TestsRecettes\Response;

/**
 * Class ApiError
 *
 * @package Was\TestsRecettes\Response
 */
class ApiError extends ErrorManager
{
    public const ERROR_GENERIC = 1;
    public const ERROR_FORM = 2;
    public const ERROR_CONFIG = 4;
    public const ERROR_DISPLAY = 8;

    protected static $errorList = [
        self::ERROR_GENERIC => 'Une erreur générique',
        self::ERROR_FORM => 'Erreur de formulaire',
        self::ERROR_CONFIG => 'Erreur de configuration',
        self::ERROR_DISPLAY => "Erreur d'affichage"
    ];

    //TODO Implémenter vos propres erreurs en ce basant sur l'existant
}