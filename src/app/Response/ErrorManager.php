<?php /** @noinspection PhpUnused */
declare(strict_types=1);

namespace Was\TestsRecettes\Response;

/**
 * Class ErrorManager
 *
 * @package Was\TestsRecettes\Response
 */
abstract class ErrorManager implements ErrorManagerInterface
{
    /** @var int $errorCode */
    protected $errorCode = 0;

    /** @var array $errorList */
    protected static $errorList = [];

    protected static $messageSeparator = ', ';

    /**
     * Retourne le message d'erreur
     */
    public function __toString()
    {
        $class = self::class;
        /** @noinspection PhpUndefinedFieldInspection */
        return implode($class::$messageSeparator, self::errorCodeToMessage($this->errorCode));
    }

    /**
     * Ajoute une nouvelle erreur à l'errorManager
     *
     * @param int $errorCode
     * @return void
     */
    public function addError(int $errorCode): void
    {
        //TODO addError
    }

    /**
     * Transforme un code en message d'erreur
     *
     * @param int $errorCode
     * @return array
     */
    public static function errorCodeToMessage(int $errorCode): array
    {
        //TODO errorCodeToMessage
    }

    //TODO hasError
}