<?php /** @noinspection PhpUnused */
declare(strict_types=1);

namespace Was\TestsRecettes\Controllers;

use Template;

/**
 * Class Shopify
 *
 * @package Was\TestRecettes\Controllers
 */
class Shopify
{
    /**
     * Effectue des opérations avant l'éxécution de la route
     */
    public function beforeroute(): void
    {
    }

    /**
     * Méthode par défaut du controlleur par défaut
     */
    public function index(): void
    {
        echo Template::instance()->render('shopify.html');
    }

    /**
     * Effectue des opérations après l'éxécution de la route
     */
    public function afterroute(): void
    {
    }
}