<?php /** @noinspection PhpUnused */
declare(strict_types=1);

namespace Was\TestsRecettes\Controllers\Api;

/**
 * Class Articles
 *
 * @package Was\TestRecettes\Controllers\Api
 */
class Articles
{
    /**
     * Effectue des opérations avant l'éxécution de la route
     */
    public function beforeroute(): void
    {
        header('Content-Type: application/json');
    }

    /**
     * Retourne la liste des articles
     */
    public function index(): void
    {
        echo json_encode([
            'status' => 'OK',
            'data' => [
                [
                    'id' => 1,
                    'name' => 'Un article au hasard',
                    'slug' => 'un-article-au-hasard',
                    'content' => 'Vestibulum arcu nisi, dapibus quis tristique a, sodales a enim. Fusce turpis ipsum, interdum sed dictum in, pellentesque vitae velit.'
                ],
                [
                    'id' => 2,
                    'name' => 'Une comète dans le ciel',
                    'slug' => 'une-comete-dans-le-ciel',
                    'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean laoreet neque ut sem pellentesque rhoncus. Morbi sed tellus sed libero lacinia laoreet.'
                ],
                [
                    'id' => 3,
                    'name' => 'Le droit et le devoir',
                    'slug' => 'le-droit-et-le-devoir',
                    'content' => 'Pellentesque vitae velit. Duis eu risus quis urna feugiat laoreet. Maecenas eget pulvinar ligula. Nulla facilisi. Aliquam erat volutpat.'
                ],
                [
                    'id' => 4,
                    'name' => 'Mon premier article',
                    'slug' => 'mon-premier-article',
                    'content' => 'Praesent luctus fermentum arcu vitae tristique. Aliquam ante ex, lobortis ut facilisis sit amet, viverra vel nisi. Fusce ac urna et felis dignissim malesuada vel id justo.'
                ]
            ]
        ]);
    }

    /**
     * Retourne un seul article
     */
    public function read(): void
    {
        // TODO Implémentation du Read de SCRUD
    }

    /**
     * Créer un nouvel article
     */
    public function create(): void
    {
        // TODO Implémentation du Create de SCRUD
    }

    /**
     * Met à jour un article
     */
    public function update(): void
    {
        // TODO Implémentation du Update de SCRUD
    }

    /**
     * Supprime un article
     */
    public function delete(): void
    {
        // TODO Implémentation du Delete de SCRUD
    }

    /**
     * Effectue des opérations après l'éxécution de la route
     */
    public function afterroute(): void
    {
    }
}