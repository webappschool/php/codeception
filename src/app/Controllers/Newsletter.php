<?php /** @noinspection PhpUnused */
declare(strict_types=1);

namespace Was\TestsRecettes\Controllers;

use Base;
use Template;

/**
 * Class Newsletter
 *
 * @package Was\TestRecettes\Controllers
 */
class Newsletter
{
    /**
     * Effectue des opérations avant l'éxécution de la route
     */
    public function beforeroute(): void
    {
    }

    /**
     * Méthode par défaut du controlleur par défaut
     */
    public function index(): void
    {
        echo Template::instance()->render('newsletter.html');
    }

    /**
     * Méthode de retour lors de la demande d'une souscription à la newsletter
     *
     * @param Base $f3
     */
    public function signin(Base $f3): void
    {
        $f3->status(201);
        echo Template::instance()->render('newsletter-signin.html');
    }

    /**
     * Effectue des opérations après l'éxécution de la route
     */
    public function afterroute(): void
    {
    }
}