<?php /** @noinspection PhpUnused */
declare(strict_types=1);

namespace Was\TestsRecettes\Security;

/**
 * Class CesarGenerator
 *
 * @package Was\TestsRecettes\Security
 */
class CesarGenerator implements CesarGeneratorInterface
{
    /**
     * CesarGenerator constructor.
     *
     * @codeCoverageIgnore
     */
    private function __construct()
    {
    }

    /**
     * Encrypte une chaîne en code Cesar
     *
     * @param string $text
     * @param int $secretNumber
     * @return string
     */
    public static function encrypt(string $text, int $secretNumber): string
    {
        //TODO encrypt
    }

    /**
     * Décrypte un chaîne en code Cesar
     *
     * @param string $text
     * @param int $secretNumber
     * @return string
     */
    public static function decrypt(string $text, int $secretNumber): string
    {
        //TODO decrypt
    }
}