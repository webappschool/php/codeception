<?php declare(strict_types=1);

namespace Was\TestsRecettes\Security;

/**
 * Interface CesarGeneratorInterface
 *
 * @package Was\TestsRecettes\Security
 */
interface CesarGeneratorInterface
{
    /**
     * Encrypte une chaîne en code Cesar
     *
     * @param string $text
     * @param int $secretNumber
     * @return string
     */
    public static function encrypt(string $text, int $secretNumber): string;

    /**
     * Décrypte un chaîne en code Cesar
     *
     * @param string $text
     * @param int $secretNumber
     * @return string
     */
    public static function decrypt(string $text, int $secretNumber): string;
}