<?php /** @noinspection PhpUnused */
declare(strict_types=1);

namespace Was\TestsRecettes\Helper;

/**
 * Class Str
 *
 * @package Was\TestsRecettes\Helper
 */
class Str implements StrInterface
{
    /**
     * Str constructor.
     */
    private function __construct()
    {
    }

    /**
     * Vérifie si le texte donnée est un palindrome
     *
     * @param string $text
     * @return bool
     */
    public static function isPalindrome(string $text): bool
    {
        //TODO isPalindrome
    }

    /**
     * Compare deux textes afin de vérifier si ce sont des anagrammes
     *
     * @param string $first
     * @param string $second
     * @return bool
     */
    public static function isAnagram(string $first, string $second): bool
    {
        //TODO isAnagram
    }

    /**
     * Incrémente un texte avec un délimiteur
     *
     * @param string $string
     * @param string $delimiter
     * @return string
     */
    public static function increment(string $string, string $delimiter = '_'): string
    {
        //TODO increment
    }

    /**
     * Décrémente un texte avec un délimiteur
     *
     * @param string $string
     * @param string $delimiter
     * @return string
     */
    public static function decrement(string $string, string $delimiter = '_'): string
    {
        //TODO decrement
    }

    //TODO countVowels

    //TODO isLower

    //TODO isUpper
}