<?php declare(strict_types=1);

namespace Was\TestsRecettes\Helper;

/**
 * Class Number
 *
 * @package Was\TestsRecettes\Helper
 */
class Number implements NumberInterface
{
    /**
     * Number constructor.
     */
    private function __construct()
    {
    }

    /**
     * Vérifie si un nombre est paire ou impaire
     *
     * @param int $number
     * @return bool
     */
    public static function isPair(int $number): bool
    {
        //TODO isPair
    }

    /**
     * Vérifie si le nombre se situe entre une limite basse et une limite haute donnée
     *
     * @param int $number
     * @param int $min
     * @param int $max
     * @return bool
     */
    public static function between(int $number, int $min, int $max): bool
    {
        //TODO between
    }
}