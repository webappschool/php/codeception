<?php declare(strict_types=1);

namespace Was\TestsRecettes\Helper;

/**
 * Interface NumberInterface
 *
 * @package Was\TestsRecettes\Helper
 */
interface NumberInterface
{
    /**
     * Vérifie si un nombre est paire ou impaire
     *
     * @param int $number
     * @return bool
     */
    public static function isPair(int $number): bool;

    /**
     * Vérifie si le nombre se situe entre une limite basse et une limite haute donnée
     *
     * @param int $number
     * @param int $min
     * @param int $max
     * @return bool
     */
    public static function between(int $number, int $min, int $max): bool;
}