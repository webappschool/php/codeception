<?php declare(strict_types=1);

namespace Was\TestsRecettes\Helper;

/**
 * Interface ArrInterface
 *
 * @package Was\TestsRecettes\Helper
 */
interface ArrInterface
{
    /**
     * Filtre un tableau via un callback
     *
     * @param array $arr
     * @param callable|null $callback
     * @return array
     */
    public static function filter(array $arr, ?callable $callback = null): array;

    /**
     * Retourne le premier élément du tableau filtrer
     *
     * @param array $arr
     * @param callable|null $callback
     */
    public static function first(array $arr, ?callable $callback = null);

    /**
     * Retourne le dernier élément du tableau filtrer
     *
     * @param array $arr
     * @param callable|null $callback
     */
    public static function last(array $arr, ?callable $callback = null);

    /**
     * Décale X élements d'un tableau donné en commençant par la gauche
     *
     * @param array $arr
     * @param int $elements
     * @return array
     */
    public static function lslice(array $arr, int $elements = 1): array;

    /**
     * Décale X élements d'un tableau donné en commençant par la droite
     *
     * @param array $arr
     * @param int $elements
     * @return array
     */
    public static function rslice(array $arr, int $elements = 1): array;
}