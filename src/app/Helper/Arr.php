<?php /** @noinspection PhpUnused */
declare(strict_types=1);

namespace Was\TestsRecettes\Helper;

/**
 * Class Arr
 *
 * @package Was\TestsRecettes\Helper
 */
class Arr implements ArrInterface
{
    /**
     * Arr constructor.
     */
    private function __construct()
    {
    }

    /**
     * Filtre un tableau via un callback
     *
     * @param array $arr
     * @param callable|null $callback
     * @return array
     */
    public static function filter(array $arr, ?callable $callback = null): array
    {
        //TODO filter
    }

    /**
     * Retourne le premier élément du tableau filtrer
     *
     * @param array $arr
     * @param callable|null $callback
     * @return mixed
     */
    public static function first(array $arr, ?callable $callback = null)
    {
        //TODO first
    }

    /**
     * Retourne le dernier élément du tableau filtrer
     *
     * @param array $arr
     * @param callable|null $callback
     * @return mixed
     */
    public static function last(array $arr, ?callable $callback = null)
    {
        //TODO last
    }

    /**
     * Décale X élements d'un tableau donné en commençant par la gauche
     *
     * @param array $arr
     * @param int $elements
     * @return array
     */
    public static function lslice(array $arr, int $elements = 1): array
    {
        //TODO lslice
    }

    /**
     * Décale X élements d'un tableau donné en commençant par la droite
     *
     * @param array $arr
     * @param int $elements
     * @return array
     */
    public static function rslice(array $arr, int $elements = 1): array
    {
        //TODO rslice
    }
}