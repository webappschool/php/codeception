<?php declare(strict_types=1);

namespace Was\TestsRecettes\Helper;

/**
 * Interface CollectionInterface
 *
 * @package Was\TestsRecettes\Helper
 */
interface CollectionInterface
{
    /**
     * Filtre les résultats d'une collection
     *
     * @param callable|null $callback
     * @return array
     */
    public function filter(?callable $callback = null): array;

    /**
     * Convertit la collection en tableau
     *
     * @return array
     */
    public function toArray(): array;
}